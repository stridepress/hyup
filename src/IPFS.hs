{-# LANGUAGE DeriveGeneric #-}

module IPFS
    ( Add(..)
    , PinLsEntry(..)
    , PinLs(..)
    , Ls(..)
    , LsObject(..)
    , LsObjectLink(..)
    , runAdd
    , runPinLs
    , runLs
    )
where

import           Data.Aeson
import qualified Data.ByteString.Lazy as BL
import           Data.Either
import           Data.HashMap.Strict
import           Data.Maybe
import           Data.Proxy
import qualified Data.Text            as T
import           GHC.Generics
import           Network.IPFS.API
import           Servant.Client

strideBaseUrl = BaseUrl Http "ipfsapi.stride.press" 80 ""

data Add =
    Add { addName :: String
        , addHash :: String
        , addSize :: String
        } deriving (Generic, Show)

data PinLsEntry =
    PinLsEntry { pinLsEntryType :: String
               } deriving (Generic, Show)

data PinLs =
    PinLs { pinLsKeys :: HashMap String PinLsEntry
          } deriving (Generic, Show)

data LsObject =
    LsObject { lsObjectHash  :: String
             , lsObjectLinks :: [LsObjectLink]
             } deriving (Generic, Show)

data LsObjectLink =
    LsObjectLink { lsObjectLinkName  :: String
                 , lsObjectLinkHash  :: String
                 , lsObjectLinkSize  :: Int
                 , lsObjectLinkType  :: Int
                 ,lsObjectLinkTarget :: String
                 } deriving (Generic, Show)

data Ls =
    Ls { lsObjects :: [LsObject]
       } deriving (Generic, Show)

genericOptions n = defaultOptions { fieldLabelModifier = drop n }

addParseOptions = genericOptions 3
pinLsParseOptions = genericOptions 5
pinLsEntryParseOptions = genericOptions 10
lsParseOptions = genericOptions 2
lsObjectOptions = genericOptions 8
lsObjectLinkOptions = genericOptions 12

instance FromJSON Add where
    parseJSON = genericParseJSON addParseOptions

instance FromJSON PinLsEntry where
    parseJSON = genericParseJSON pinLsEntryParseOptions

instance FromJSON PinLs where
    parseJSON = genericParseJSON pinLsParseOptions

instance FromJSON Ls where
    parseJSON = genericParseJSON lsParseOptions

instance FromJSON LsObject where
    parseJSON = genericParseJSON lsObjectOptions

instance FromJSON LsObjectLink where
    parseJSON = genericParseJSON lsObjectLinkOptions

add'   = client (Proxy :: Proxy ApiV0Add)
pinLs' = client (Proxy :: Proxy ApiV0PinLs)
ls'    = client (Proxy :: Proxy ApiV0Ls)

add :: BL.ByteString -> ClientM (Result Add)
add bs = add'' bs >>= return . fromJSON
    where
        add'' bs' =
            add' bs'        -- data
                Nothing     -- recursive
                Nothing     -- quiet
                Nothing     -- quieter
                Nothing     -- silent
                Nothing     -- progress
                Nothing     -- trickle
                Nothing     -- only-hash
                Nothing     -- wrap-with-directory
                Nothing     -- hidden
                Nothing     -- chunker
                (Just True) -- pin
                Nothing     -- raw-leaves
                Nothing     -- nocopy
                Nothing     -- fscache
                Nothing     -- cid-version
                Nothing     -- hash function

pinLs :: ClientM (Result PinLs)
pinLs = pinLs'' >>= return . fromJSON
    where
        pinLs'' =
            pinLs'
                Nothing
                (Just $ T.pack "recursive")
                (Just True)

ls :: String -> ClientM (Result Ls)
ls h = ls'' >>= return . fromJSON
    where
        ls'' =
            ls'
                (T.pack h)
                Nothing
                Nothing


runGeneric f m = runClientM f (mkClientEnv m strideBaseUrl)

runPinLs m = runGeneric pinLs   m
runAdd m c = runGeneric (add c) m
runLs  m h = runGeneric (ls h)  m
