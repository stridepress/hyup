module Main where

import           App
import           Auth
import           Config
import           Data.Cache               (Cache, newCache)
import           Data.Vault.Lazy          (Key, newKey)
import           Network.HTTP.Client      (defaultManagerSettings, newManager)
import           Network.Wai.Handler.Warp

main :: IO ()
main = do
    manager <- newManager defaultManagerSettings
    authMiddleware <- mkMiddleware
    config <- loadConfig
    cache <- newCache Nothing
    putStrLn $ "running on localhost:" <> (show $ port config) <> "..."
    app (manager, authMiddleware, cache) >>= run (fromInteger . toInteger $ port config)
