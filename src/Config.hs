{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Config where

import           Dhall

data Config
    = Config
        { port :: Natural
        } deriving (Generic, Show)

instance Interpret Config

loadConfig :: IO Config
loadConfig = input auto "./config.dhall"
