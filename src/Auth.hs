{-# LANGUAGE OverloadedStrings #-}

module Auth(mkMiddleware) where

import           Data.HashMap.Lazy                    (fromList)
import           Network.Wai.Middleware.Auth
import           Network.Wai.Middleware.Auth.OAuth2
import           Network.Wai.Middleware.Auth.Provider

cloakProviderInfo = ProviderInfo "Stride" "https://cloak.stride.press/auth/resources/6.0.1/admin/keycloak/img/keyclok-logo.png" "Keycloak"

authEndpoint  = "https://cloak.stride.press/auth/realms/master/protocol/openid-connect/auth"
tokenEndpoint = "https://cloak.stride.press/auth/realms/master/protocol/openid-connect/token"

cloakProvider =
    OAuth2 { oa2ClientId = "haskell"
           , oa2ClientSecret = "10ca4e72-6edb-428c-9c31-d257bdf8c831"
           , oa2AuthorizeEndpoint = authEndpoint
           , oa2AccessTokenEndpoint = tokenEndpoint
           , oa2Scope = Just [ "openid" ]
           , oa2ProviderInfo = cloakProviderInfo
           }

providers = fromList [("oauth2", Provider cloakProvider)]

settings = setAuthProviders providers
         $ defaultAuthSettings

mkMiddleware = mkAuthMiddleware settings
