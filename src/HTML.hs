module HTML where

data Node
    = Node Bool String [(String, String)] [Node]
    | Text String

renderNode' :: Int -> Node -> String
renderNode' level (Node noclose name props children)
    = "\n"
   <> (indent level)
   <> "<"
   <> name
   <> (foldl (<>) "" $ map (\(n, v) -> " " <> n <> "=\"" <> v <> "\" ") props)
   <> ">\n"
   <> (indent (level + 1))
   <> (foldl (<>) "" $ map (renderNode' (level + 1)) children)
   <> "\n"
   <> (indent level)
   <> if noclose
      then ""
      else "</"
        <> name
        <> ">"
    where
        indent level = replicate (level * 2) ' '
renderNode' level (Text str)
    = (indent level)
   <> str
   where
    indent level = replicate (level * 2) ' '

renderNode :: Node -> String
renderNode node = renderNode' 0 node

renderHTML :: Node -> String
renderHTML node
    = "<!DOCTYPE html>"
   <> renderNode node

[html, head, body, ul, li, title, h1, legend, span]
    = fmap (\n -> Node False n [])
        ["html", "head", "body", "ul", "li", "title", "h1", "legend", "span"]

form = Node False "form"
a = Node False "a"
link = Node True "link"
meta = Node True "meta"
img = Node True "img"
fieldset = Node False "fieldset"
hr = Node True "hr" [] []
input = (flip $ Node True "input") []
