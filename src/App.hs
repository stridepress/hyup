{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module App (app) where

import           Control.Monad.IO.Class
import           Data.Aeson.Types          (Result (Success))
import qualified Data.ByteString.Char8     as BS
import           Data.Cache
import qualified Data.HashMap.Strict       as H
import           Data.Maybe
import qualified Data.Text.Lazy            as T
import qualified Data.Vault.Lazy           as V
import qualified HTML                      as M
import           IPFS
import           Network.HTTP.Types.Status
import           Network.Wai
import           Network.Wai.Parse
import           Web.Scotty

gatewayUrl = "https://ipfs.stride.press/ipfs/"

nameFromLs :: (Either a (Result Ls)) -> String
nameFromLs lsRoot = case lsRoot of
    Right (Success lr) -> case lsObjects lr of
        [] -> "unknown"
        objs -> case lsObjectLinks $ head objs of
            []    -> "unknown"
            links -> if length name == 0
                     then "unknown"
                     else name
                        where
                            name = lsObjectLinkName $ head links
    _ -> "unknown"

commonHead :: [M.Node]
commonHead
    = [ M.title [ M.Text "StrideUp" ]
      , M.link [("href", "https://unpkg.com/normalize.css@8.0.1/normalize.css"), ("rel", "stylesheet")] []
      , M.link [("href", "https://unpkg.com/sakura.css/css/sakura-vader.css"), ("rel", "stylesheet")] []
      ]

indexHtml :: M.Node
indexHtml
    = M.html
    [ M.head ([] ++ commonHead)
    , M.body
        [ M.h1 [ M.Text "StrideUp" ]
        , M.hr
        , M.img [("src", "https://i.imgur.com/eyt2dtC.jpg")] []
        , M.form [("action", "/"), ("enctype", "multipart/form-data"), ("method", "POST")]
            [ M.fieldset [("id", "fieldset")]
                [ M.legend [M.Text "Uploader"]
                , M.input [("type", "file"), ("id", "file"), ("name", "file")]
                , M.input [("type", "submit")]
                ]
            ]
        , M.a [("href", "/pins")] [ M.Text "Pinned files" ]

        ]
    ]

pinsHtml :: [(String, String)] -> M.Node
pinsHtml pins
    = M.html
    [ M.head ([] ++ commonHead)
    , M.body
        [
            M.ul $ map (\(hash, name) ->
                M.li [ M.a [("href", gatewayUrl ++ hash)] [M.Text name] ]) pins
        ]
    ]

app (manager, authMiddleware, cache) = scottyApp $ do
    middleware authMiddleware

    get "/" $ html . T.pack $ M.renderHTML indexHtml

    post "/" $ do
        f <- files
        if length f == 0
        then status status400 >> text "no u"
        else do
            let fileInfo = snd . head $ f
                content  = fileContent fileInfo
            addResp <- liftIO $ runAdd manager content
            case addResp of
                    Right (Success add') -> redirect
                                          . T.pack
                                          $ gatewayUrl
                                         ++ addHash add'
                    _                    -> raise "What the fuck dude"

    get "/pins" $ do
        pinLsResp <- liftIO $ runPinLs manager
        req <- request
        let pins = case pinLsResp of
                Right (Success pins') -> H.keys . pinLsKeys $ pins'
                _                     -> []
        names <- mapM (\h -> do
            n <- liftIO $ Data.Cache.lookup cache h
            name <- case n of
                Just name -> return name
                Nothing   -> do
                    lsRoot <- liftIO $ runLs manager h
                    liftIO $ Data.Cache.insert cache h (nameFromLs lsRoot)
                    return $ nameFromLs lsRoot
            return (h, name)) pins
        html . T.pack . M.renderHTML . pinsHtml $ names


