{ nixpkgs ? import <nixpkgs> {}, compiler ? "default", doBenchmark ? false }:

let

  inherit (nixpkgs) pkgs;

  f = { mkDerivation, aeson, base, bytestring, cache, http-client
      , http-types, scotty, servant-client, stdenv, text
      , unordered-containers, vault, wai, wai-extra, wai-middleware-auth
      , wai-middleware-static, warp, dhall
      }:
      mkDerivation {
        pname = "strideup";
        version = "0.1.0.0";
        src = ./.;
        isLibrary = false;
        isExecutable = true;
        executableHaskellDepends = [
          aeson base bytestring cache http-client http-types scotty
          servant-client text unordered-containers vault wai wai-extra
          wai-middleware-auth wai-middleware-static warp dhall
        ];
        license = stdenv.lib.licenses.bsd3;
      };

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;

  drv = variant (haskellPackages.callPackage f {});

in

  if pkgs.lib.inNixShell then drv.env else drv
